import NthFibonacci from "./fibonacci.js";

describe('Nth Fibbonacci tests', () => {
    test('1th fibonacci number should be 0', () => {
      const fibonacci = new NthFibonacci(1);
      expect(fibonacci.nthFibonacci()).toEqual(0);
    }); 
    test('2th fibonacci number should be 1', () => {
      const fibonacci = new NthFibonacci(2);
      expect(fibonacci.nthFibonacci()).toEqual(1);
    }); 
    test('3th fibonacci number should be 1', () => {
      const fibonacci = new NthFibonacci(3);
      expect(fibonacci.nthFibonacci()).toEqual(1);
    }); 
    test('4th fibonacci number should be 2', () => {
      const fibonacci = new NthFibonacci(4);
      expect(fibonacci.nthFibonacci()).toEqual(2);
    }); 
    test('5th fibonacci number should be 3', () => {
      const fibonacci = new NthFibonacci(5);
      expect(fibonacci.nthFibonacci()).toEqual(3);
    }); 
    test('6th fibonacci number should be 5', () => {
      const fibonacci = new NthFibonacci(6);
      expect(fibonacci.nthFibonacci()).toEqual(5);
    }); 
    test('10th fibonacci number should be 34', () => {
      const fibonacci = new NthFibonacci(10);
      expect(fibonacci.nthFibonacci()).toEqual(34);
    }); 
    test('15th fibonacci number should be 377', () => {
      const fibonacci = new NthFibonacci(15);
      expect(fibonacci.nthFibonacci()).toEqual(377);
    }); 
    test('19th fibonacci number should be 2584', () => {
      const fibonacci = new NthFibonacci(19);
      expect(fibonacci.nthFibonacci()).toEqual(2584);
    }); 
    test('25th fibonacci number should be 463684', () => {
      const fibonacci = new NthFibonacci(25);
      expect(fibonacci.nthFibonacci()).toEqual(46368);
    });
});
